
<?php
echo "Soal Nomor 1 <br> <br>";
function tentukan_nilai($number)
{
    //  kode disini
    if($number>=80){
        echo "Sangat Baik <br>";
    }else if($number>=70){
        echo "Baik <br>";
    }else if($number>=60){
        echo "Cukup <br>";
    } else if ($number < 60) {
        echo"Kurang <br> <br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang



echo "Soal Nomor 2 <br> <br>";

function ubah_huruf($string)
{
    $abjad = "abcdefghijklmnopqrstuwxyz";
    $output = "";

    for($i=0;$i<strlen($string);$i++){
        $position = strpos($abjad, $string[$i]);
        $output .= substr($abjad, $position+1, 1);
    }
    return $output . "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
echo "<br>";

echo "Soal Nomor 3 <br> <br>";

function tukar_besar_kecil($string)
{
    //kode di sini
    for ($i = 0; $i < strlen($string); $i++) {
        if(ctype_upper($string[$i])){
            $output=strtolower($string[$i]);
            
        } else {
            $output=strtoupper($string[$i]);
        }
        echo "$output";
        
    }

    echo "<br>";
    

    
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
?>